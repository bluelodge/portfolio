$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 1800
    });
    $('#contacto').on('show.bs.modal', function(event){
        console.log('modal contacto se está mostrando');
        $('.contactoBtn').removeClass('btarjetas');
        $('.contactoBtn').addClass('balt');
        $('.contactoBtn').prop('disabled', true);
        var button = $(event.relatedTarget);
        var ph = button.data('foto')
        var modal = $(this)
        modal.find('.modal-title').text('Formulario cotización para Producción de ' + ph);
        
    });
    $('#contacto').on('shown.bs.modal', function(e){
        console.log('modal contacto se mostró');        
    });
    $('#contacto').on('hide.bs.modal', function(e){
        console.log('modal contacto se está ocultando');
        $('.contactoBtn').removeClass('balt');
        $('.contactoBtn').addClass('btarjetas');
        $('.contactoBtn').prop('disabled', false);
    });
    $('#contacto').on('hidden.bs.modal', function(e){
        console.log('modal contacto se ocultó');        
    });
    
})